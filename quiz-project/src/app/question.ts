import { Option } from './option';

export class Question {
    id: number;
    question: string;
    options: Option[];

    constructor(data: any) {
        this.id = data.id;
        this.question = data.question;
        this.options = [];
        data.options.forEach(eachOption => {
            var newOption = new Option(eachOption);
            this.options.push(newOption);
        });

    }

}
